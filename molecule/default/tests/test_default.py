import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_packages_available(host):
    p = host.Package('restic')
    assert p.is_installed


def test_for_user(host):
    u = host.User('restic_testuser')
    assert u.home == '/home/restic_testuser'


@pytest.mark.parametrize("path", [
    '/home/restic_testuser/.config/restic',
    '/home/restic_testuser/.config/systemd/user'
    '/home/restic_testuser/.config/restic/exclude',
    '/home/restic_testuser/.config/restic/config',
    '/home/restic_testuser/.config/systemd/user/restic_backup.service',
    '/home/restic_testuser/.config/systemd/user/restic_backup.timer',
    '/home/restic_testuser/.config/systemd/user/restic_prune.service'
])
def test_config_file(host, path):
    f = host.file(path)
    assert f.exists
    assert f.user == 'restic_testuser'
    if path == '/home/restic_testuser/.config/restic' or \
            path == '/home/restic_testuser/.config/systemd/user':
        assert f.is_directory
        assert f.mode == 0o700
    else:
        assert f.is_file
        assert f.mode == 0o400
    if path == '/home/restic_testuser/.config/restic/exclude':
        assert f.contains('.cache')
    if path == '/home/restic_testuser/.config/restic/config':
        assert f.contains("^BACKUP_PATH='/home/restic_testuser'")


@pytest.mark.parametrize("service", [
    'restic_backup.service',
    'restic_backup.timer',
    'restic_prune.service',
    'restic_prune.timer'
])
def test_service_definitions(host, service):
    s = host.Service(service)
    assert s.is_enabled
